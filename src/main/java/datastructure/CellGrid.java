package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int columns;
    int rows;
    CellState [][] currentState;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.columns = columns;
        this.rows = rows;
        currentState = new CellState [rows][columns];
        for (int row = 0; row < currentState.length; row++) {
            for (int col = 0; col < currentState[row].length; col++) {
                set(row, col, initialState);
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        currentState[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub

        return currentState[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid nextGrid = new CellGrid(rows, columns, CellState.DEAD);
        for (int row = 0; row < currentState.length; row++) {
            for (int col = 0; col < currentState[row].length; col++) {
                nextGrid.set(row, col, get(row, col));
            }
        }
        return nextGrid;
    }
    
}
